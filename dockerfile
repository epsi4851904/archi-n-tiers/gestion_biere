FROM php:8.2-apache-buster

RUN apt-get update && apt-get install -y \
    libicu-dev \
    zip \
    unzip \
    libpng-dev \
    zlib1g-dev \
    libzip-dev \
    && docker-php-ext-install pdo_mysql intl gd zip

COPY . /var/www/html

COPY apache.conf /etc/apache2/sites-available/000-default.conf

WORKDIR /var/www/html

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN a2enmod rewrite headers
