# APP GESTION BIERE

## Schéma :

Retrouvez les images du schéma à la racine du projet

## Installation :

A la racine du projet faire la commande :

```shell
docker compose up -d --build

docker exec -it symfony_app bash

php bin/console d:m:m

php bin/console doctrine:fixtures:load
```

Retrouver l'app ici : http://localhost

Retrouver phpMyadmin ici : http://localhost:8080

Pour se connecter à l'app 2 comptes :

Admin:
- username: `admin`
- password: `admin1234@`

Serveur:
- username: `serveur`
- password: `serveur1234@`

## Info utile :

A la route : http://localhost/admin/fournisseur/1/edit

Il est possible d'importer un stock de bière grâce au fichier stock.csv présent à la racine du projet qui pourra être utilisé à cette fonction