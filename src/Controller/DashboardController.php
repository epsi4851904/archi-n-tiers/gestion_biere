<?php

namespace App\Controller;

use App\DBAL\Types\StatusOrderType;
use App\Entity\Biere;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Produit;
use App\Repository\BiereRepository;
use App\Repository\OrderRepository;
use App\Repository\ProduitRepository;
use App\Service\OrderingService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DashboardController extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    #[Route('/dashboard', name: 'app_dashboard')]
    public function index(BiereRepository $biereRepository, Request $request): Response
    {


        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController'
        ]);
    }

    #[Route('/caisse', name: 'app_caisse')]
    public function caisse(BiereRepository $biereRepository, Request $request): Response
    {
        $bieres = $biereRepository->findAll();

        if ($request->request->get('biere')) {
            $biere = $biereRepository->find($request->request->get('biere'));
            if ($biere instanceof Biere) {
                $stock = $biere->getQuantity() >= 0 ? $biere->getQuantity() - 1 : 0;
                $biere->setQuantity($stock);

                $this->entityManager->persist($biere);
                $this->entityManager->flush();
            }
        }

        return $this->render('dashboard/caisse.html.twig', [
            'controller_name' => 'DashboardController',
            'bieres' => $bieres
        ]);
    }

    #[Route('/admin/stock', name: 'stock_app')]
    public function stock(BiereRepository $biereRepository): Response
    {
        $bieres = $biereRepository->findAll();

        return $this->render('dashboard/stock.html.twig', [
            'controller_name' => 'DashboardController',
            'bieres' => $bieres
        ]);
    }
}
