<?php

namespace App\Controller;

use App\Entity\Biere;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;

class CatalogueController extends AbstractController
{

    #[Route('/catalog', name: 'app_catalog_list')]
    public function index(ProduitRepository $produitRepository)
    {
        $produits = $produitRepository->findAll();

        return $this->render('catalog/list.html.twig', [
            'produits' => $produits
        ]);
    }
}