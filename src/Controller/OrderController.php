<?php

namespace App\Controller;

use App\DBAL\Types\StatusOrderType;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Produit;
use App\Repository\OrderRepository;
use App\Repository\ProduitRepository;
use App\Service\OrderingService;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('admin/order')]
class OrderController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route('', name: 'app_list_order')]
    public function list(OrderRepository $orderRepository)
    {
        $orders = $orderRepository->findAll();

        return $this->render('order/list.html.twig', [
            'orders' => $orders
        ]);
    }
    #[Route('/new', name: 'reapro_app')]
    public function reapro(OrderingService $orderingService, ProduitRepository $produitRepository): Response
    {
        $order = $orderingService->getLastOrder();
        $price = $orderingService->getTotal($order);

        $produits = $produitRepository->findAll();

        return $this->render('dashboard/reapro_bis.html.twig', [
            'controller_name' => 'DashboardController',
            'order' => $order,
            'price' => $price,
            'produits' => $produits
        ]);
    }

    #[Route('/validate', name: 'app_order_validate')]
    public function validate(OrderingService $orderingService, ProduitRepository $produitRepository)
    {
        $order = $orderingService->getLastOrder();
        $order->setStatus(StatusOrderType::COMMANDER);
        $order->setOrderedAt(new \DateTime());
        $order->setEstimLivraisonAt((new \DateTime())->modify('+10 days'));

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return $this->redirectToRoute('app_order_by_id', [
            'order' => $order->getId()
        ], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{order}', name: 'app_order_by_id')]
    public function viewById(Order $order, OrderingService $orderingService, ProduitRepository $produitRepository): Response
    {
        if ($order->getStatus() === StatusOrderType::EN_COURS) {
            return $this->redirectToRoute('reapro_app', [], Response::HTTP_SEE_OTHER);
        }
        $price = $orderingService->getTotal($order);

        $produits = $produitRepository->findAll();

        return $this->render('order/view.html.twig', [
            'controller_name' => 'DashboardController',
            'order' => $order,
            'price' => $price,
            'produits' => $produits
        ]);
    }

    #[Route('/{order}/{id}/add', name: 'app_add_produit')]
    public function add(Order $order, Produit $id, OrderingService $orderingService)
    {
        $orderingService->add($order, $id);

        return $this->redirectToRoute('reapro_app', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{order}/{id}/delete', name: 'app_order_item_delete')]
    public function delete(Order $order, OrderItem $id)
    {
        $order->removeOrderItem($id);
        $this->entityManager->remove($id);
        $this->entityManager->flush();

        return $this->redirectToRoute('reapro_app', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{orderItem}/add', name: 'app_add_quantity')]
    public function addQuantity(OrderItem $orderItem)
    {
        $orderItem->setQuantity($orderItem->getQuantity() + 1);

        $this->entityManager->persist($orderItem);
        $this->entityManager->flush();

        return $this->redirectToRoute('reapro_app', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{orderItem}/remove', name: 'app_remove_quantity')]
    public function removeQuantity(OrderItem $orderItem)
    {
        $orderItem->setQuantity($orderItem->getQuantity() - 1);
        $this->entityManager->persist($orderItem);
        $this->entityManager->flush();

        if ($orderItem->getQuantity() === 0) {
            $this->entityManager->remove($orderItem);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('reapro_app', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{order}/reception', name: 'app_order_reception')]
    public function reception(Order $order, ProduitRepository $produitRepository)
    {
        if ($order->getStatus() !== StatusOrderType::COMMANDER) {
            return $this->redirectToRoute('app_order_by_id', [
                'order' => $order->getId(),
                'erreur' => 'Commande pas encore validé'
            ], Response::HTTP_SEE_OTHER);
        }

        $order->setStatus(StatusOrderType::RECU);
        $order->setReceptionedAt(new \DateTime());
        $this->entityManager->persist($order);
        foreach ($order->getOrderItems() as $orderItem) {
            /** @var OrderItem $orderItem */

            $produit = $produitRepository->find($orderItem->getProduit()->getId());
            $quantity = $produit->getBiere()->getQuantity() + $orderItem->getQuantity();
            $biere = $produit->getBiere();
            $biere->setQuantity($quantity);

            $this->entityManager->persist($biere);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('app_order_by_id', [
            'order' => $order->getId()
        ], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{order}/pdf', name: 'app_order_pdf')]
    public function pdf(Order $order, OrderingService $orderingService)
    {
        $fournisseur = $order->getOrderItems()->first()->getProduit()->getFournisseur();
        $price = $orderingService->getTotal($order);
        $html = $this->renderView('pdf/order.html.twig', [
            'order' => $order,
            'fournisseur' => $fournisseur,
            'price' => $price
        ]);

        $pdf = new Dompdf();
        $pdf->loadHtml($html);
        $pdf->setPaper('A4', 'portrait');

        $pdf->render();

        return new Response($pdf->output(), Response::HTTP_OK, [
            'Content-Type' => 'application/pdf',
            'Content-disposition' => 'inline; filename="bon_commande.pdf}"'
        ]);
    }
}