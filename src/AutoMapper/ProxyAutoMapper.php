<?php

namespace App\AutoMapper;

use AutoMapperPlus\AutoMapper;

class ProxyAutoMapper extends AutoMapper
{
    public function map($source, string $destinationClass, array $context = [])
    {
        if (method_exists($source, '__load')) {
            $source->__load();
        }

        return parent::map($source, $destinationClass, $context);
    }
}