<?php

namespace App\AutoMapper\BierePassionFournisseur;

use App\Entity\Produit;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\DataType;
use AutoMapperPlus\MappingOperation\Operation;

class ProductConfig implements AutoMapperConfiguratorInterface
{

    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(DataType::ARRAY, Produit::class)
            ->withDefaultOperation(Operation::ignore())
            ->dontSkipConstructor()
            ->forMember('priceHt', function ($sv) {
                return $sv['price_ht'];
            })
            ->forMember('priceTtc', function ($sv) {
                return $sv['price_ttc'];
            })
            ->forMember('ref', function ($sv) {
                return $sv['ref'];
            })
        ;
    }
}