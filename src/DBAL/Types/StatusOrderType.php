<?php

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class StatusOrderType extends AbstractEnumType
{
    public const EN_COURS = 'en-cours';
    public const COMMANDER = 'commander';
    public const RECU = 'recu';


    public static array $choices = [
        self::EN_COURS => 'En cours',
        self::COMMANDER => 'Commande passée',
        self::RECU => 'Commande reçu'
    ];

}