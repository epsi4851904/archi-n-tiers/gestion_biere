<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    use TimestampableEntity;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'StatusOrderType')]
    private $status = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    protected $orderedAt;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    protected $receptionedAt;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    protected $estimLivraisonAt;

    /**
     * @var Collection<int, OrderItem>
     */
    #[ORM\OneToMany(targetEntity: OrderItem::class, mappedBy: 'ordering')]
    private Collection $orderItems;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status): static
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, OrderItem>
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): static
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems->add($orderItem);
            $orderItem->setOrdering($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): static
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getOrdering() === $this) {
                $orderItem->setOrdering(null);
            }
        }

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getOrderedAt()
    {
        return $this->orderedAt;
    }

    public function setOrderedAt(\DateTime $date): static
    {
        $this->orderedAt = $date;

        return $this;
    }

    public function getReceptionedAt()
    {
        return $this->receptionedAt;
    }

    public function setReceptionedAt(\DateTime $date): static
    {
        $this->receptionedAt = $date;

        return $this;
    }

    public function getEstimLivraisonAt()
    {
        return $this->estimLivraisonAt;
    }

    public function setEstimLivraisonAt(\DateTime $date): static
    {
        $this->estimLivraisonAt = $date;

        return $this;
    }
}
