<?php

namespace App\DataFixtures;

use App\Entity\Biere;
use App\Entity\Fournisseur;
use App\Entity\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FournisseurFixtures extends Fixture
{
    public const FOURNISSEURS = [
        [
            'name' => 'Bière shop',
            'phone' => '0678903456',
            'route' => '9 route du soleil',
            'zipcode' => '33300',
            'country' => 'France',
            'hasApi' => false
        ]
    ];

    public const PRODUITS = [
        [
            'ref' => 'PROD001',
            'priceHt' => 10.25,
            'priceTtc' => 12.30,
            'name' => 'Delirium Red'
        ],
        [
            'ref' => 'PROD002',
            'priceHt' => 15.75,
            'priceTtc' => 18.90,
            'name' => 'Paix dieu'
        ],
        [
            'ref' => 'PROD003',
            'priceHt' => 20.50,
            'priceTtc' => 24.60,
            'name' => 'Hoogardeen blanche'
        ],
        [
            'ref' => 'PROD004',
            'priceHt' => 8.95,
            'priceTtc' => 10.74,
            'name' => 'Goudale',
        ],
    ];
    public function load(ObjectManager $manager)
    {
        foreach (self::FOURNISSEURS as $item) {
            $fournisseur = new Fournisseur();
            foreach ($item as $property => $value) {
                $method = 'set' . ucfirst($property);
                if (method_exists($fournisseur, $method)) {
                    $fournisseur->$method($value);
                }
            }
            $manager->persist($fournisseur);

            foreach (self::PRODUITS as $itemProduit) {
                $produit = new Produit();
                foreach ($itemProduit as $property => $value) {
                    $method = 'set' . ucfirst($property);
                    if (method_exists($produit, $method)) {
                        $produit->$method($value);
                    }
                }
                $produit->setBiere($manager->getRepository(Biere::class)->findOneBy(['name' => $itemProduit['name']]));
                $produit->setFournisseur($fournisseur);
                $manager->persist($produit);
            }
        }
        $manager->flush();
    }
}