<?php

namespace App\DataFixtures;

use App\Entity\Biere;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BiereFixtures extends Fixture
{
    public const DATA = [
        [
            'name' => 'Delirium Red',
            'quantity' => 10,
            'limitQuantity' => 8,
            'urlImage' => 'https://selfdrinks.com/24181/delirium-red-fut-5l.jpg'
        ],
        [
            'name' => 'Goudale',
            'quantity' => 20,
            'limitQuantity' => 15,
            'urlImage' => 'https://media.carrefour.fr/medias/20bb682c83cc34e9ab8df1e7c8594402/p_540x540/03261570002086-c1n1-s01.jpg'
        ],
        [
            'name' => 'Paix dieu',
            'quantity' => 10,
            'limitQuantity' => 8,
            'urlImage' => 'https://festicave.com/media/catalog/product/cache/0f89a8ae84232c76845fec4b739964fb/p/a/paix_dieu_.jpg'
        ],
        [
            'name' => 'Hoogardeen blanche',
            'quantity' => 20,
            'limitQuantity' => 10,
            'urlImage' => 'https://www.achat-bieres.com/3804-large_default/mini-fut-hoegaarden-blanche-6-litres-perfect-draft-brasserie-inbev.jpg'
        ]
    ];
    public function load(ObjectManager $manager)
    {
        foreach (self::DATA as $item) {
            $biere = new Biere();
            foreach ($item as $property => $value) {
                $method = 'set' . ucfirst($property);
                if (method_exists($biere, $method)) {
                    $biere->$method($value);
                }
            }
            $manager->persist($biere);
        }
        $manager->flush();
    }
}