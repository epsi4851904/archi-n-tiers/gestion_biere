<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public function __construct(private readonly UserPasswordHasherInterface $passwordHasher)
    {
    }
    public function load(ObjectManager $manager): void
    {
        $admin = new User();
        $serveur = new User();

        $admin->setUsername('admin')
            ->setPassword($this->passwordHasher->hashPassword($admin, 'admin1234@'))
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER']);
        $serveur->setUsername('serveur')
            ->setPassword($this->passwordHasher->hashPassword($serveur, 'serveur1234@'))
            ->setRoles(['ROLE_SERVEUR', 'ROLE_USER']);

        $manager->persist($admin);
        $manager->persist($serveur);
        $manager->flush();
    }
}
