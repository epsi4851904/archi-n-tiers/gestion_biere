<?php

namespace App\Service\StrategyImportFournisseur;

use App\AutoMapper\BierePassionFournisseur\ProductConfig;
use App\Repository\BiereRepository;
use App\Repository\ProduitRepository;
use AutoMapperPlus\Configuration\AutoMapperConfig;

class ImportBierePassion
{
    public function __construct(
        private readonly ProduitRepository $produitRepository,
        private readonly BiereRepository $biereRepository
    ) {
    }
    public function getAutoMapper()
    {
        $config = new AutoMapperConfig();
        (new ProductConfig())->configure($config);

        return $config;
    }

    public function getProduct($product, $fournisseur)
    {
        return $this->produitRepository->findOneBy(['fournisseur' => $fournisseur, 'ref' => $product['ref']]);
    }

    public function getBiere($product)
    {
        return $this->biereRepository->findBiereByName($product['name']);
    }
}