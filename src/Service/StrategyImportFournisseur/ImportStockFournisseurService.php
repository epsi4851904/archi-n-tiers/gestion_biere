<?php

namespace App\Service\StrategyImportFournisseur;

use App\AutoMapper\ProxyAutoMapper;
use App\Entity\Biere;
use App\Entity\Fournisseur;
use App\Entity\Produit;
use AutoMapperPlus\AutoMapper;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use RuntimeException;

class ImportStockFournisseurService
{
    private $fournisseur;
    private $autoMapper;
    public function __construct(
        private readonly ImportBierePassion $bierePassion,
        private readonly EntityManagerInterface $entityManager
    ) {
    }
    public function getFournisseur(Fournisseur $fournisseur)
    {
        switch ($fournisseur->getId()) {
            case 1:
                return $this->bierePassion;
            default:
                throw new RuntimeException(
                    'Le nom fourni est invalide',
                );
        }
    }
    public function importCSV($file, Fournisseur $fournisseur)
    {
        $this->fournisseur = $this->getFournisseur($fournisseur);

        $csv = Reader::createFromPath($file, 'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);

        $records = $csv->getRecords();

        foreach ($records as $record) {

            $product = $this->fournisseur->getProduct($record, $fournisseur);

            if ($product instanceof Produit) {
                $produit = $this->update($product, $record);
            } else {
                $produit = $this->create($record, $fournisseur);
            }

            $this->entityManager->persist($produit);
            $this->entityManager->flush();
        }
    }

    public function create($product, $fournisseur): Produit
    {
        $biere = $this->fournisseur->getBiere($product);

        if (!$biere instanceof Biere) {
            $biere = new Biere();
            $biere->setName($product['name']);
            $this->entityManager->persist($biere);
        }

        $this->autoMapper = new AutoMapper($this->fournisseur->getAutoMapper());

        /** @var Produit $produit */
        $produit = $this->autoMapper->map($product, Produit::class);
        $produit->setBiere($biere)
            ->setFournisseur($fournisseur);

        return $produit;
    }

    public function update($produit, $product): Produit
    {
        $this->autoMapper = new ProxyAutoMapper($this->fournisseur->getAutoMapper());
        $this->autoMapper->mapToObject($product, $produit);

        return $produit;
    }
}