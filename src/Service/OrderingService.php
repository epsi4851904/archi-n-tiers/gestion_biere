<?php

namespace App\Service;

use App\DBAL\Types\StatusOrderType;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Produit;
use App\Repository\OrderRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderingService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ProduitRepository $produitRepository,
        private readonly OrderRepository $orderRepository
    ) {
    }

    public function getLastOrder(): Order
    {
        $order = $this->orderRepository->findOneBy([
            'status' => StatusOrderType::EN_COURS
        ]);

        if (!$order instanceof Order) {
            $order = $this->create();

            return $order;
        }
        $this->update($order);

        return $order;
    }

    public function create(): Order
    {
        $order = new Order();
        $order->setStatus(StatusOrderType::EN_COURS);
        $this->entityManager->persist($order);
        $this->entityManager->flush();

        $produits = $this->produitRepository->getBiereForReapro();

        foreach ($produits as $produit) {
            $orderItem = new OrderItem();
            $orderItem->setProduit($produit)
                ->setQuantity($produit->getQuantityToReapro())
                ->setOrdering($order);

            $order->addOrderItem($orderItem);
            $this->entityManager->persist($orderItem);
            $this->entityManager->persist($order);
            $this->entityManager->flush();
        }

        return $order;
    }

    public function update(Order $order)
    {
        $produits = $this->produitRepository->getBiereForReapro();

        foreach ($produits as $produit) {
            $orderItem = $order->getOrderItems()->filter(function ($item) use ($produit) {
                return $item->getProduit()->getId() === $produit->getId();
            })->first();

            if ($orderItem instanceof OrderItem) {
                $quantity = $produit->getQuantityToReapro() > $orderItem->getQuantity()
                    ? $produit->getQuantityToReapro()
                    : $orderItem->getQuantity();
                $orderItem->setQuantity($quantity);
            } else {
                $orderItem = new OrderItem();
                $orderItem->setProduit($produit)
                    ->setQuantity($produit->getQuantityToReapro())
                    ->setOrdering($order);
            }

            $this->entityManager->persist($orderItem);
            $this->entityManager->flush();
        }
    }

    public function getTotal(Order $order): array
    {
        $totalTTC = 0;
        $totalHT = 0;
        foreach ($order->getOrderItems() as $orderItem) {
            $totalTTC += $orderItem->getProduit()->getPriceTtc() * $orderItem->getQuantity();
            $totalHT += $orderItem->getProduit()->getPriceHt() * $orderItem->getQuantity();
        }

        return [
            'totalHT' => $totalHT,
            'totalTTC' => $totalTTC
        ];
    }

    public function add(Order $order, Produit $produit): Order
    {
        $orderItem = $order->getOrderItems()->filter(function ($item) use ($produit) {
            return $item->getProduit()->getId() === $produit->getId();
        })->first();

        if ($orderItem instanceof OrderItem) {
            return $order;
        }

        $orderItem = new OrderItem();
        $quantity = $produit->getQuantityToReapro() <= 0 ? 1 : $produit->getQuantityToReapro();
        $orderItem->setProduit($produit)
            ->setQuantity($quantity)
            ->setOrdering($order);

        $this->entityManager->persist($orderItem);
        $this->entityManager->flush();

        return $order;
    }
}