<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240523160111 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` CHANGE ordered_at ordered_at DATETIME DEFAULT NULL, CHANGE receptioned_at receptioned_at DATETIME DEFAULT NULL, CHANGE estim_livraison_at estim_livraison_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `order` CHANGE ordered_at ordered_at DATETIME NOT NULL, CHANGE receptioned_at receptioned_at DATETIME NOT NULL, CHANGE estim_livraison_at estim_livraison_at DATETIME NOT NULL');
    }
}
